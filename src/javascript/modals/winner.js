import {showModal} from './modal';
import {createElement} from "../helpers/domHelper";
import {createImage} from "../fighterView";

export function showWinnerModal(fighter) {
    const {name, source} = fighter;

    const title = `${name} WON!`;
    const bodyElement = createElement({tagName: 'div', className: 'modal-body'});

    const imageElement = createImage(source);
    bodyElement.append(imageElement);

    showModal({title, bodyElement});
}