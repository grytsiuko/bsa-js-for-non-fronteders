import {createElement} from '../helpers/domHelper';
import {showModal} from './modal';
import {createImage, createTableRow} from '../fighterView';

export function showFighterDetailsModal(fighter) {
    const title = 'Fighter info';
    const bodyElement = createFighterDetails(fighter);
    showModal({title, bodyElement});
}

function createFighterDetails(fighter) {
    const {name, attack, defense, health, source} = fighter;

    const fighterDetails = createElement({tagName: 'div', className: 'modal-body'});
    const modalTable = createElement({tagName: 'table', className: 'modal-table'});

    const nameElement = createTableRow('Name', name);
    const attackElement = createTableRow('Attack', attack);
    const defenseElement = createTableRow('Defense', defense);
    const healthElement = createTableRow('Health', health);

    modalTable.append(nameElement, attackElement, defenseElement, healthElement);
    const imageElement = createImage(source);

    fighterDetails.append(modalTable, imageElement);
    return fighterDetails;
}
