import {callApi} from '../helpers/apiHelper';

export async function getFighters() {
    try {
        const endpoint = 'fighters.json';
        return await callApi(endpoint, 'GET');;
    } catch (error) {
        throw error;
    }
}

export async function getFighterDetails(id) {
    try {
        const endpoint = `details/fighter/${id}.json`;
        return await callApi(endpoint, 'GET');
    } catch (error) {
        throw error;
    }
}

