export function fight(firstFighter, secondFighter) {
    let attacker = firstFighter;
    let defender = secondFighter;

    attacker.leftHP = attacker.health;
    defender.leftHP = defender.health;

    while (attacker.leftHP > 0 && defender.leftHP > 0) {
        makeMove(attacker, defender);
        [attacker, defender] = [defender, attacker];
    }

    return defender;
}

function makeMove(attacker, defender) {
    const damage = getDamage(attacker, defender);
    defender.leftHP -= damage;
}

export function getDamage(attacker, enemy) {
    const hit = getHitPower(attacker);
    const block = getBlockPower(enemy);

    return Math.max(hit - block, 0);
}

export function getHitPower(fighter) {
    const {attack} = fighter;
    const criticalHitChance = Math.random() + 1;

    return attack * criticalHitChance;
}

export function getBlockPower(fighter) {
    const {defense} = fighter;
    const dodgeChance = Math.random() + 1;

    return defense * dodgeChance;
}
